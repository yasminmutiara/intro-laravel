@extends('layout.master')


  @section('content')
  <div class="card card-primary">
    <div class="card-header">
      <h3 class="card-title">Creat new cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form role="form" action="{{ route('cast.store') }}" method="POST">
        @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="nama">Nama</label>
          <input type="text" class="form-control" id="nama" name= "nama" value="{{ old('nama', '') }}" placeholder="Enter name" required>
          @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
        </div>
        <div class="form-group">
          <label for="umur">Umur</label>
          <input type="text" class="form-control" id="umur" name= "umur" value="{{ old('umur', '') }}" placeholder="Umur" required>
          @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <input type="text" class="form-control" id="bio" name= "bio" value="{{ old('bio', '') }}" placeholder="bio" required>
            @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
          </div>
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary">Submit</button>
      </div>
    </form>
  </div>
 @endsection