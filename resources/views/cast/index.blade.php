@extends('layout.master')


  @section('content')
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Cast Table</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      @push('script')
<script>
    Swal.fire({
        title: "Berhasil!",
        icon: "success",
        confirmButtonText: "Cool",
    });
</script>

    
@endpush
      <a class="btn btn-primary mb-2" href="{{ route('cast.create') }}">Create New Cast</a>
      <table class="table table-bordered">
        <thead>
          <tr>
            <th style="width: 10px">No</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th style="width: 40px">Action</th>
          </tr>
        </thead>
        <tbody>
          @forelse($cast as $key => $casts)
            <td> {{$key + 1}} </td>
            <td> {{ $casts->nama }}</td>
            <td> {{ $casts->umur }}</td>
            <td> {{ $casts->bio }}</td>
            <td style="display: flex">
              <a href="{{ route('cast.show', ['cast' => $casts->id])}}" class="btn btn-info btn-sm">Show</a>
              <a href="{{ route('cast.edit', ['cast' => $casts->id])}}" class="btn btn-default btn-sm">Edit</a>
              <form action="{{ route('cast.destroy', ['cast' => $casts->id])}}" method="post">
                @csrf
                @method('DELETE')
                <input type="submit" value="delete" class="btn btn-danger btn-sm">
              </form>
            </td>
          </tr>
          @empty
          <tr>
              <td colspan="4" align="center"> No create </td>
             
             </tr> 
          @endforelse
      </table>
      </div>      
    <!-- /.card-body -->
    
  </div>
  @endsection