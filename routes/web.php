<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', 'HomeController@home');
//Route::get('/register', 'AuthController@register');
//Route::post('/welcome', 'AuthController@welcome');

//Route::get('/data-table', function(){
    //return view('halaman.data-table');

//});

//Route::get('/table', function(){
    //return view('halaman.table');

//});

//Route::get('/cast/create', 'CastController@create');
//Route::post('/cast', 'CastController@store');
//Route::get('/cast', 'CastController@index');
//Route::get('/cast/{id}', 'CastController@show');
//Route::get('/cast/{id}/edit', 'CastController@edit');
//Route::put('/cast/{id}', 'CastController@update');
//Route::delete('/cast/{id}', 'CastController@destroy');

Route::resource('cast', 'PostController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

