<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome(Request $request){
        $nama = $request['first'];
        $nama2 = $request['last'];

        return view('halaman.Welcome', compact('nama','nama2'));
    }
}
