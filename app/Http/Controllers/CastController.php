<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;
class CastController extends Controller
{
    public function create(){
        return view('cast.create');
    }

    public function store(Request $request){
        //dd($request->all());

        $request->validate([
            'nama' => 'required|unique:cast,nama',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //$query = DB::table('cast')->insert([
           // "nama" => $request["nama"],
            //"umur" => $request["umur"],
            //"bio" => $request["bio"]
        //]);

        $cast = new Cast;
        $cast->nama =  $request["nama"];
        $cast->umur =  $request["umur"];
        $cast->bio =  $request["bio"];
        $cast->save();
        return redirect('/cast')->with('success', 'Update Berhasil Dilakukan ');
    }

    public function index(){
        //$obi = DB::table('cast')->get();
        //dd($obi);

        $cast = Cast::all();
        return view('cast.index', compact('cast'));
    }

    public function show($id){
        //$ool = DB::table('cast')->where('id', $id)->first();
        //dd($ool);
        $cast = Cast::find($id);
        return view('cast.show', compact('cast'));
    }

    public function edit($id){
        //$yuu = DB::table('cast')->where('id', $id)->first();
        $cast = Cast::find($id);
        return view('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required|unique:cast,nama',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //$jaja = DB::table('cast')
                //->where('id', $id)
                //->update([
                    //'nama' => $request['nama'],
                    //'umur' => $request['umur'],
                    //'bio'  => $request['bio']
               // ]);

               $update = Cast::where('id', $id)->update([
                   "nama" => $request["nama"],
                   "umur" => $request["umur"],
                   "bio" => $request["bio"]
               ]);
                return redirect('/cast')->with('success', 'Update Berhasil Dilakukan ');
    }

    public function destroy($id){
        //$haha = DB::table('cast')->where('id', $id)->delete();
        Cast::destroy($id);
    
        return redirect('/cast')->with('success', 'Cast Berhasil Dihapus ');
    }
}
